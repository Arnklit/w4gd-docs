:github_url: hide

.. DO NOT EDIT THIS FILE!!!
.. Generated automatically from W4GD sources.
.. GDScript source: https://gitlab.com/W4Games/w4gd/-/blob/main/w4rm/w4rm_types.gd.

.. _class_addons_w4gd_w4rm_w4rm_types_gd_w4rmtabletype:

res://addons/w4gd/w4rm/w4rm_types.gd:W4RMTableType
==================================================

**Inherits:** :ref:`res://addons/w4gd/w4rm/w4rm_types.gd:W4RMCompositeType<class_addons_w4gd_w4rm_w4rm_types_gd_w4rmcompositetype>` **<** :ref:`res://addons/w4gd/w4rm/w4rm_types.gd:W4RMType<class_addons_w4gd_w4rm_w4rm_types_gd_w4rmtype>` **<** RefCounted

.. container:: contribute

	There is currently no description for this class.

.. rst-class:: classref-reftable-group

Properties
----------

.. table::
   :widths: auto

   +------------+-------------------------------------------------------------------------------------+---------+
   | StringName | :ref:`id_name<class_addons_w4gd_w4rm_w4rm_types_gd_w4rmtabletype_property_id_name>` | ``&""`` |
   +------------+-------------------------------------------------------------------------------------+---------+

.. rst-class:: classref-reftable-group

Methods
-------

.. table::
   :widths: auto

   +------------+-----------------------------------------------------------------------------------------------------------------+
   | StringName | :ref:`get_id_type_name<class_addons_w4gd_w4rm_w4rm_types_gd_w4rmtabletype_method_get_id_type_name>` **(** **)** |
   +------------+-----------------------------------------------------------------------------------------------------------------+

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Property Descriptions
---------------------

.. _class_addons_w4gd_w4rm_w4rm_types_gd_w4rmtabletype_property_id_name:

.. rst-class:: classref-property

StringName **id_name** = ``&""``

.. container:: contribute

	There is currently no description for this property.

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Method Descriptions
-------------------

.. _class_addons_w4gd_w4rm_w4rm_types_gd_w4rmtabletype_method_get_id_type_name:

.. rst-class:: classref-method

StringName **get_id_type_name** **(** **)**

.. container:: contribute

	There is currently no description for this method.

.. |virtual| replace:: :abbr:`virtual (This method should typically be overridden by the user to have any effect.)`
.. |const| replace:: :abbr:`const (This method has no side effects. It doesn't modify any of the instance's member variables.)`
.. |vararg| replace:: :abbr:`vararg (This method accepts any number of arguments after the ones described here.)`
.. |constructor| replace:: :abbr:`constructor (This method is used to construct a type.)`
.. |static| replace:: :abbr:`static (This method doesn't need an instance to be called, so it can be called directly using the class name.)`
.. |operator| replace:: :abbr:`operator (This method describes a valid operator to use with this type as left-hand operand.)`
