:github_url: hide

.. DO NOT EDIT THIS FILE!!!
.. Generated automatically from W4GD sources.
.. GDScript source: https://gitlab.com/W4Games/w4gd/-/blob/main/w4rm/w4rm_table.gd.

.. _class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy:

res://addons/w4gd/w4rm/w4rm_table.gd:W4RMTable.SecurityPolicy
=============================================================

**Inherits:** RefCounted

.. container:: contribute

	There is currently no description for this class.

.. rst-class:: classref-reftable-group

Properties
----------

.. table::
   :widths: auto

   +--------+------------------------------------------------------------------------------------------------------+--------+
   | int    | :ref:`command<class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_command>`       |        |
   +--------+------------------------------------------------------------------------------------------------------+--------+
   | Array  | :ref:`roles<class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_roles>`           |        |
   +--------+------------------------------------------------------------------------------------------------------+--------+
   | int    | :ref:`type<class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_type>`             |        |
   +--------+------------------------------------------------------------------------------------------------------+--------+
   | String | :ref:`using<class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_using>`           | ``""`` |
   +--------+------------------------------------------------------------------------------------------------------+--------+
   | String | :ref:`with_check<class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_with_check>` | ``""`` |
   +--------+------------------------------------------------------------------------------------------------------+--------+

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Property Descriptions
---------------------

.. _class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_command:

.. rst-class:: classref-property

int **command**

.. container:: contribute

	There is currently no description for this property.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_roles:

.. rst-class:: classref-property

Array **roles**

.. container:: contribute

	There is currently no description for this property.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_type:

.. rst-class:: classref-property

int **type**

.. container:: contribute

	There is currently no description for this property.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_using:

.. rst-class:: classref-property

String **using** = ``""``

.. container:: contribute

	There is currently no description for this property.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_w4rm_w4rm_table_gd_w4rmtable_securitypolicy_property_with_check:

.. rst-class:: classref-property

String **with_check** = ``""``

.. container:: contribute

	There is currently no description for this property.

.. |virtual| replace:: :abbr:`virtual (This method should typically be overridden by the user to have any effect.)`
.. |const| replace:: :abbr:`const (This method has no side effects. It doesn't modify any of the instance's member variables.)`
.. |vararg| replace:: :abbr:`vararg (This method accepts any number of arguments after the ones described here.)`
.. |constructor| replace:: :abbr:`constructor (This method is used to construct a type.)`
.. |static| replace:: :abbr:`static (This method doesn't need an instance to be called, so it can be called directly using the class name.)`
.. |operator| replace:: :abbr:`operator (This method describes a valid operator to use with this type as left-hand operand.)`
