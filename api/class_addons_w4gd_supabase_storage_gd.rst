:github_url: hide

.. DO NOT EDIT THIS FILE!!!
.. Generated automatically from W4GD sources.
.. GDScript source: https://gitlab.com/W4Games/w4gd/-/blob/main/supabase/storage.gd.

.. _class_addons_w4gd_supabase_storage_gd:

res://addons/w4gd/supabase/storage.gd
=====================================

**Inherits:** :ref:`res://addons/w4gd/supabase/endpoint.gd<class_addons_w4gd_supabase_endpoint_gd>` **<** RefCounted

The Supabase Storage end-point at /storage/v1.

.. rst-class:: classref-reftable-group

Methods
-------

.. table::
   :widths: auto

   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`copy_object<class_addons_w4gd_supabase_storage_gd_method_copy_object>` **(** String p_bucket, String p_source, String p_destination **)**                                                                                                          |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`create_bucket<class_addons_w4gd_supabase_storage_gd_method_create_bucket>` **(** String p_name, bool p_public, String p_id **)**                                                                                                                   |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`create_object_url<class_addons_w4gd_supabase_storage_gd_method_create_object_url>` **(** String p_bucket, String p_path, int p_expires_in **)**                                                                                                    |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`delete_bucket<class_addons_w4gd_supabase_storage_gd_method_delete_bucket>` **(** String p_name **)**                                                                                                                                               |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`delete_object<class_addons_w4gd_supabase_storage_gd_method_delete_object>` **(** String p_bucket, String p_path **)**                                                                                                                              |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`delete_objects<class_addons_w4gd_supabase_storage_gd_method_delete_objects>` **(** String p_bucket, Array p_prefixes **)**                                                                                                                         |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`empty_bucket<class_addons_w4gd_supabase_storage_gd_method_empty_bucket>` **(** String p_name **)**                                                                                                                                                 |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_bucket<class_addons_w4gd_supabase_storage_gd_method_get_bucket>` **(** String p_name **)**                                                                                                                                                     |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_object<class_addons_w4gd_supabase_storage_gd_method_get_object>` **(** String p_bucket, String p_path **)**                                                                                                                                    |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_object_auth<class_addons_w4gd_supabase_storage_gd_method_get_object_auth>` **(** String p_bucket, String p_path **)**                                                                                                                          |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_object_from_url<class_addons_w4gd_supabase_storage_gd_method_get_object_from_url>` **(** String p_bucket, String p_path, String p_token **)**                                                                                                  |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_object_info<class_addons_w4gd_supabase_storage_gd_method_get_object_info>` **(** String p_bucket, String p_path **)**                                                                                                                          |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_object_info_auth<class_addons_w4gd_supabase_storage_gd_method_get_object_info_auth>` **(** String p_bucket, String p_path **)**                                                                                                                |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_object_info_public<class_addons_w4gd_supabase_storage_gd_method_get_object_info_public>` **(** String p_bucket, String p_path **)**                                                                                                            |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_object_public<class_addons_w4gd_supabase_storage_gd_method_get_object_public>` **(** String p_bucket, String p_path **)**                                                                                                                      |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`get_status<class_addons_w4gd_supabase_storage_gd_method_get_status>` **(** **)**                                                                                                                                                                   |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`list_buckets<class_addons_w4gd_supabase_storage_gd_method_list_buckets>` **(** **)**                                                                                                                                                               |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`list_objects<class_addons_w4gd_supabase_storage_gd_method_list_objects>` **(** String p_bucket, String p_prefix, int p_limit, int p_offset, String p_sort_col, :ref:`SortOrder<enum_addons_w4gd_supabase_storage_gd_SortOrder>` p_sort_order **)** |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`move_object<class_addons_w4gd_supabase_storage_gd_method_move_object>` **(** String p_bucket, String p_source, String p_destination **)**                                                                                                          |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`update_bucket<class_addons_w4gd_supabase_storage_gd_method_update_bucket>` **(** String p_name, bool p_public **)**                                                                                                                                |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`update_object<class_addons_w4gd_supabase_storage_gd_method_update_object>` **(** String p_bucket, String p_path, Variant p_data, String p_mime **)**                                                                                               |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`upload_object<class_addons_w4gd_supabase_storage_gd_method_upload_object>` **(** String p_bucket, String p_path, Variant p_data, String p_mime **)**                                                                                               |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
   | void | :ref:`upsert_object<class_addons_w4gd_supabase_storage_gd_method_upsert_object>` **(** String p_bucket, String p_path, Variant p_data, String p_mime **)**                                                                                               |
   +------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Enumerations
------------

.. _enum_addons_w4gd_supabase_storage_gd_SortOrder:

.. rst-class:: classref-enumeration

enum **SortOrder**:

.. _class_addons_w4gd_supabase_storage_gd_constant_ASC:

.. rst-class:: classref-enumeration-constant

:ref:`SortOrder<enum_addons_w4gd_supabase_storage_gd_SortOrder>` **ASC** = ``0``



.. _class_addons_w4gd_supabase_storage_gd_constant_DESC:

.. rst-class:: classref-enumeration-constant

:ref:`SortOrder<enum_addons_w4gd_supabase_storage_gd_SortOrder>` **DESC** = ``1``



.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Method Descriptions
-------------------

.. _class_addons_w4gd_supabase_storage_gd_method_copy_object:

.. rst-class:: classref-method

void **copy_object** **(** String p_bucket, String p_source, String p_destination **)**

Copies the given object.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_create_bucket:

.. rst-class:: classref-method

void **create_bucket** **(** String p_name, bool p_public, String p_id **)**

Creates a new bucket with the given name.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_create_object_url:

.. rst-class:: classref-method

void **create_object_url** **(** String p_bucket, String p_path, int p_expires_in **)**

Creates a publicly sharable "signed URL" for the given object.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_delete_bucket:

.. rst-class:: classref-method

void **delete_bucket** **(** String p_name **)**

Deletes the given bucket.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_delete_object:

.. rst-class:: classref-method

void **delete_object** **(** String p_bucket, String p_path **)**

Deletes the given object.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_delete_objects:

.. rst-class:: classref-method

void **delete_objects** **(** String p_bucket, Array p_prefixes **)**

Deletes multiple objects.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_empty_bucket:

.. rst-class:: classref-method

void **empty_bucket** **(** String p_name **)**

Empties the given bucket.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_bucket:

.. rst-class:: classref-method

void **get_bucket** **(** String p_name **)**

Gets the given bucket.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_object:

.. rst-class:: classref-method

void **get_object** **(** String p_bucket, String p_path **)**

Gets an object.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_object_auth:

.. rst-class:: classref-method

void **get_object_auth** **(** String p_bucket, String p_path **)**

Gets an object as an authenticated user.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_object_from_url:

.. rst-class:: classref-method

void **get_object_from_url** **(** String p_bucket, String p_path, String p_token **)**

Gets an object via a "signed URL".

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_object_info:

.. rst-class:: classref-method

void **get_object_info** **(** String p_bucket, String p_path **)**

Gets an object's info.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_object_info_auth:

.. rst-class:: classref-method

void **get_object_info_auth** **(** String p_bucket, String p_path **)**

Gets an object's info as an authenticated user.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_object_info_public:

.. rst-class:: classref-method

void **get_object_info_public** **(** String p_bucket, String p_path **)**

Gets an object's info as an anonymous user.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_object_public:

.. rst-class:: classref-method

void **get_object_public** **(** String p_bucket, String p_path **)**

Gets an object as an anonymous user.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_get_status:

.. rst-class:: classref-method

void **get_status** **(** **)**

Checks the status of the storage service.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_list_buckets:

.. rst-class:: classref-method

void **list_buckets** **(** **)**

List all buckets that the current user has access to.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_list_objects:

.. rst-class:: classref-method

void **list_objects** **(** String p_bucket, String p_prefix, int p_limit, int p_offset, String p_sort_col, :ref:`SortOrder<enum_addons_w4gd_supabase_storage_gd_SortOrder>` p_sort_order **)**

Lists objects.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_move_object:

.. rst-class:: classref-method

void **move_object** **(** String p_bucket, String p_source, String p_destination **)**

Moves the given object.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_update_bucket:

.. rst-class:: classref-method

void **update_bucket** **(** String p_name, bool p_public **)**

Updates the given bucket.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_update_object:

.. rst-class:: classref-method

void **update_object** **(** String p_bucket, String p_path, Variant p_data, String p_mime **)**

Updates the given object.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_upload_object:

.. rst-class:: classref-method

void **upload_object** **(** String p_bucket, String p_path, Variant p_data, String p_mime **)**

Uploads a new object.

.. rst-class:: classref-item-separator

----

.. _class_addons_w4gd_supabase_storage_gd_method_upsert_object:

.. rst-class:: classref-method

void **upsert_object** **(** String p_bucket, String p_path, Variant p_data, String p_mime **)**

Uploads a new object or updates an existing one if it already exists.

.. |virtual| replace:: :abbr:`virtual (This method should typically be overridden by the user to have any effect.)`
.. |const| replace:: :abbr:`const (This method has no side effects. It doesn't modify any of the instance's member variables.)`
.. |vararg| replace:: :abbr:`vararg (This method accepts any number of arguments after the ones described here.)`
.. |constructor| replace:: :abbr:`constructor (This method is used to construct a type.)`
.. |static| replace:: :abbr:`static (This method doesn't need an instance to be called, so it can be called directly using the class name.)`
.. |operator| replace:: :abbr:`operator (This method describes a valid operator to use with this type as left-hand operand.)`
