:github_url: hide

.. DO NOT EDIT THIS FILE!!!
.. Generated automatically from W4GD sources.
.. GDScript source: https://gitlab.com/W4Games/w4gd/-/blob/main/W4Utils.UUIDGenerator.

.. _class_w4utils.uuidgenerator:

W4Utils.UUIDGenerator
=====================

**Inherits:** RefCounted

Internal class used internally to generate v4 UUIDs.

.. rst-class:: classref-reftable-group

Properties
----------

.. table::
   :widths: auto

   +-----------------------+------------------------------------------------------------+
   | Crypto                | :ref:`crypto<class_w4utils.uuidgenerator_property_crypto>` |
   +-----------------------+------------------------------------------------------------+
   | RandomNumberGenerator | :ref:`rng<class_w4utils.uuidgenerator_property_rng>`       |
   +-----------------------+------------------------------------------------------------+

.. rst-class:: classref-reftable-group

Methods
-------

.. table::
   :widths: auto

   +--------+--------------------------------------------------------------------------------+
   | String | :ref:`generate_v4<class_w4utils.uuidgenerator_method_generate_v4>` **(** **)** |
   +--------+--------------------------------------------------------------------------------+

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Property Descriptions
---------------------

.. _class_w4utils.uuidgenerator_property_crypto:

.. rst-class:: classref-property

Crypto **crypto**

.. container:: contribute

	There is currently no description for this property.

.. rst-class:: classref-item-separator

----

.. _class_w4utils.uuidgenerator_property_rng:

.. rst-class:: classref-property

RandomNumberGenerator **rng**

.. container:: contribute

	There is currently no description for this property.

.. rst-class:: classref-section-separator

----

.. rst-class:: classref-descriptions-group

Method Descriptions
-------------------

.. _class_w4utils.uuidgenerator_method_generate_v4:

.. rst-class:: classref-method

String **generate_v4** **(** **)**

.. container:: contribute

	There is currently no description for this method.

.. |virtual| replace:: :abbr:`virtual (This method should typically be overridden by the user to have any effect.)`
.. |const| replace:: :abbr:`const (This method has no side effects. It doesn't modify any of the instance's member variables.)`
.. |vararg| replace:: :abbr:`vararg (This method accepts any number of arguments after the ones described here.)`
.. |constructor| replace:: :abbr:`constructor (This method is used to construct a type.)`
.. |static| replace:: :abbr:`static (This method doesn't need an instance to be called, so it can be called directly using the class name.)`
.. |operator| replace:: :abbr:`operator (This method describes a valid operator to use with this type as left-hand operand.)`
