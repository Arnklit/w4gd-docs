:github_url: hide

.. DO NOT EDIT THIS FILE!!!
.. Generated automatically from W4GD sources.
.. GDScript source: https://gitlab.com/W4Games/w4gd/-/blob/main/W4Utils.

.. _class_w4utils:

W4Utils
=======

**Inherits:** RefCounted

.. container:: contribute

	There is currently no description for this class.

.. |virtual| replace:: :abbr:`virtual (This method should typically be overridden by the user to have any effect.)`
.. |const| replace:: :abbr:`const (This method has no side effects. It doesn't modify any of the instance's member variables.)`
.. |vararg| replace:: :abbr:`vararg (This method accepts any number of arguments after the ones described here.)`
.. |constructor| replace:: :abbr:`constructor (This method is used to construct a type.)`
.. |static| replace:: :abbr:`static (This method doesn't need an instance to be called, so it can be called directly using the class name.)`
.. |operator| replace:: :abbr:`operator (This method describes a valid operator to use with this type as left-hand operand.)`
