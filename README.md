W4 Cloud Documentation
======================

This is the source code for the W4 Cloud documentation.

It's a mix of handwritten tutorials in reStructuredText for Sphinx, and API documentation that's generated automatically from inline comments in the W4GD source code.

One-Time Setup
==============

1. Install [pyenv](https://github.com/pyenv/pyenv#installation) to help ensure you're on the same Python version as the rest of the team:

    ```bash
    # You can use your systems package manager, or run the installer script like this:
    curl https://pyenv.run | bash

    # Be sure to add the required code to your .bashrc or other relevant shell files:
    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
    echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(pyenv init -)"' >> ~/.bashrc

    # Close your shell and start a new one.
    ```

    If use another shell or a more advanced configuration, see the [pyenv docs for setting up your shell environment](https://github.com/pyenv/pyenv#set-up-your-shell-environment-for-pyenv).

2. Use pyenv and check that its working:

    ```bash
    python --version
    ```

    This should output a message like:

    ```
    pyenv: version `3.10.6' is not installed (set by /home/user/prj/w4games/w4kube/tests/.python-version)
    ```

    This is good! It means pyenv is working, but it doesn't have the right version installed yet.

    Run this command to install it:

    ```bash
    pyenv install
    ```

    Then try running `python --version` again, and ensure that it shows the same version as listed in `.python-version`.

    If pyenv doesn't seem to be working, try logging out of your system and logging back in again. If that doesn't work, consult the [pyenv documentation](https://github.com/pyenv/pyenv).

3. Setup a Python virtual env with all the necessary dependencies:

    ```bash
    python -m venv python.env
    . python.env/bin/activate
    pip install -r requirements.txt
    ```

Generating API documentation
----------------------------

The script that generates the API documentation needs a Godot project with W4GD installed. If you don't have one, the script will generate one in the `api_project/` directory. Keep this in mind, if you want to update W4GD.

Here's how you run the script:

```bash
# Only needs to be done once in this shell session.
. python.env/bin/activate

PROJECT="/home/user/prj/w4games/My W4GD Test Project" ./build_api_docs.sh
```

Simply omit the `PROJECT` variable, if you want it to be generated.

NOTE: This depends on Godot 4.1 or later. If you need to use a custom version of Godot, set the `GODOT` environment variable with the path to the Godot executable.

Build HTML pages
----------------

After you've made any changes to the reStructuredText (either manually or via the `./build_api_docs.sh` script), run this command to build the HTML pages:

```bash
# Only needs to be done once in this shell session.
. python.env/bin/activate

make html
```

Then you can open `_build/html/index.html` in your web browser!
