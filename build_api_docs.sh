#!/bin/bash

GODOT=${GODOT:-godot}
PROJECT=${PROJECT:-}

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

die() {
    echo "$@" >/dev/stderr
    exit 1
}

# Make a default project to hold the w4gd addon.
if [ -z "$PROJECT" ]; then
    PROJECT="$SCRIPT_DIR/api_project"
    if [ ! -d "$PROJECT" ]; then
        mkdir -p "$PROJECT/addons"
        (cd "$PROJECT/addons" && git clone https://gitlab.com/W4Games/sdk/w4gd.git) \
            || die "Failed to clone w4gd repo"
        cat << EOF > "$PROJECT/project.godot"
config_version=5

[application]

config/features=PackedStringArray("4.1")

[autoload]

W4GD="*res://addons/w4gd/w4gd.gd"

[editor_plugins]

enabled=PackedStringArray("res://addons/w4gd/plugin/plugin.cfg")

[w4games]

game_server/enabled=true
EOF
        # Run the editor twice to "prime" the project.
        $GODOT --headless --editor --path "$PROJECT" --quit >/dev/null 2>&1
        $GODOT --headless --editor --path "$PROJECT" --quit >/dev/null 2>&1
        echo " ** Project created: ${PROJECT}"
    fi
fi

# Regenerate the XML data from the API.
rm -rf "$SCRIPT_DIR/api_xml"
$GODOT --doctool "$SCRIPT_DIR/api_xml" --path "$PROJECT" --gdscript-docs addons/w4gd \
    || die "Failed to regenerate XML from the inline docs"

# Regenerate the RST files from the XML.
rm -rf "$SCRIPT_DIR/api"
"$SCRIPT_DIR/make_rst.py" "$SCRIPT_DIR/api_xml" --output "$SCRIPT_DIR/api" \
    || die "Failed to regenerate RST from the XML"
