.. _doc_getting_started_database_storage:

Storage
=======

.. warning:: Just an outline!

- Wanna store cloud saves for your players?
- Or, other content in the cloud that your game client downloads?
- Hey, we got a thing for that!
