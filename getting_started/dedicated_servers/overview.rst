.. _doc_getting_started_dedicated_servers_overview:

Overview
========

We've built our system on top of an Open Source game server orchestration platform called `Agones <https://agones.dev/>`_.

Agones is well-suited to games that have distinct "matches" or "sessions" that start, run for a short period of time (minutes to hours) and then stop. For example, a battle royale game where 50 players join a single session, play for 1 hour and then leave to join a different match.

There's a couple of key concepts:

Game Server
    An instance of your Godot game, exported for Linux, and configured to act as a dedicated server for a single "match" or "session" of your game.

Fleet
    A collection of Game Servers running the same game build and configuration.

    You can set the minimum and maximum number of Game Servers that can be created in a Fleet, as well as the number of extra game servers to keep ready (the "buffer size").

    The actual number of Game Servers will automatically scale up and down in response to player demand.

Region
    A cluster of physical servers in a particular region of the world, that the Game Servers run on.

    Each Fleet is assigned a Region, allowing you to have different builds or configurations in each Region, as well as different rules for the number of Game Servers.

