.. _doc_getting_started_matchmaker_basics:

Lobby basics
============

A lobby represents a collection of players who are in (or about to be in) a match together.

Creating a lobby
----------------

After :ref:`authenticating the player<doc_getting_started_authentication>`, a new lobby can be created like this::

    var result = await W4GD.matchmaker.create_lobby(
        W4GD.matchmaker.LobbyType.LOBBY_ONLY,
        {
            # You can use any properties for the lobby that make sense for your game!
            "game_mode": "battle-royale",
        }
    ).async()

    if result.is_error():
        print("ERROR: ", result.message)
        return

    var lobby = result.get_data()

    print("Created lobby with ID: ", lobby.id)

See :ref:`class_addons_w4gd_matchmaker_matchmaker_gd_lobby` for all of a lobby's members and methods.

Joining a lobby
---------------

Another player can join the lobby using its ID::

    var result = await W4GD.matchmaker.join_lobby('xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx').async()

    if result.is_error():
        print("ERROR: ", result.message)
        return

    var lobby = result.get_data()

    print("Join lobby with ID: ", lobby.id)

    print("Players in this lobby:")
    for player_id in lobby.get_players():
        print(" - ", player_id)

Lobby IDs are UUIDs, usually represented by 36 characters strings, which may be longer than players are willing to type.

So, you may want to consider implementing either:

#. An invitation system where players invite their friends, and the UUIDs are transferred behind the scenes (such that players never see them), or

#. A lobby "short name" system, where there is a lookup table connecting the UUIDs to shorter and more human friendly names.

Either of these could easily be implemented in the database using :ref:`doc_getting_started_database_w4rm`.

See :ref:`Example: User Profiles<doc_getting_started_database_examples_user_profiles>`.

Updating or deleting a lobby
----------------------------

The player that created the lobby can freely update or delete the lobby until it is "sealed"::

    lobby.props['game_mode'] = 'deathmatch'

    var result = await lobby.save()
    if result.is_error():
        print("ERROR: ", result.message)
        return

Other players can connect to signals on the lobby to be notified when it's been updated or deleted::

    lobby.updated.connect(self._on_lobby_updated)
    lobby.deleted.connect(self._on_lobby_deleted)

Lobby states
------------

A lobby can exist in one of four states:

- NEW
- IN_PROGRESS
- SEALED
- DONE

The first 3 can be used in any way that makes sense for your game.

However, once a lobby enters the ``SEALED`` state:

#. Players can no longer join or leave the lobby.

#. The lobby can no longer be updated or deleted.

The player that created the lobby can update its state::

    # Let's set the lobby status to SEALED so the match can start!
    lobby.state = W4GD.matchmaker.LobbyState.SEALED

    var result = await lobby.save()
    if result.is_error():
        print("ERROR: ", result.message)
        return

Listing the players in a lobby
------------------------------

Players in a lobby can see a list of the other players::

    for user_id in lobby.get_players():
        print("User ID: ", user_id)

This list should be automatically updated when first joining a lobby and as other players join or leave. However, you can also manually request that it be refreshed::

    var result = await lobby.refresh_player_list()
    if result.is_error():
        print("ERROR: ", result.message)
        return

    var player_list = result.get_data()

Players are identified by their user ID, which like lobby ID's, is a UUID represented as a 36 character string.

If you'd like to show human-readable user names or pictures, this can be easily implemented in the database using :ref:`doc_getting_started_database_w4rm`.

See :ref:`Example: Lobby Invitations or Codes<doc_getting_started_database_examples_lobby_invitations_or_codes>`.
