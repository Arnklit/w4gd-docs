.. _doc_getting_started_matchmaker_index:

Matchmaker
==========

W4 Cloud features a lobby system which supports:

- Manually creating and joining lobbies
- A "matchmaking queue" that players can join and be automatically placed a lobby
- WebRTC signalling for peer-to-peer games
- Automatically allocating :ref:`dedicated servers<doc_getting_started_dedicated_servers>` for authoritative games

Collectively, the component that provides these features is called **the Matchmaker.**

.. toctree::
   :maxdepth: 1

   basics
   automated
   webrtc
