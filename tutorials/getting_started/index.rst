.. _doc_tutorials_getting_started_index:

Getting started with W4 Cloud
=============================

Welcome to the W4 Cloud intro tutorial!

In this Getting Started series, you will learn how to use W4 Cloud for
multiple people to play online together using the W4 API.

.. toctree::
   :maxdepth: 1

   010.intro
   020.how-w4-multiplayer-works
   030.exploring-start-project
   040.installing-w4-addon
   050.authenticating-players
   060.database-management
   070.matchmaking-and-lobbies
   080.hosting-the-game
   090.joining-game-rounds
   100.finishing-game-round
   110.connecting-essential-signals
   120.exporting-and-uploading-game
   bonus_versioning
   annex
   summary_of_how_w4_works

