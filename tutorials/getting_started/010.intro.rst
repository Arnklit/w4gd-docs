.. _doc_tutorials_getting_started_introduction:

What you'll learn in this series
================================

Welcome to the W4 Cloud intro tutorial!

In this Getting Started series, you will learn how to use W4 Cloud for
multiple people to play online together using the W4 API.

.. raw:: html

   <video width="100%" height="320" controls>
   <source src="../../_static/video/tutorial_getting_started_preview.mp4" type="video/mp4">
   </video>

We'll add W4 Cloud's multiplayer to a game inspired by
`Hex-A-Gone <https://fallguysultimateknockout.fandom.com/wiki/Hex-A-Gone>`__
from `Fall Guys <https://www.fallguys.com>`__.

The game's rules are simple:

-  When you walk on a hexagon, after a few moments, it falls.
-  If you fall, you lose.
-  If you bump into another player, you push them.
-  The last person to stand wins.

We'll learn the parts specific to W4 Cloud. This series assumes you
already have at least some game development experience and are familiar
with Godot.

We will follow these broad steps:

1. First, we will add the necessary code for the game to use W4's
   services.
2. Then, we will create a server version of the game.
3. Finally, we will upload it on W4 and prepare it to run online.

Creating online multiplayer games requires good programming foundations
already. If you're not confident in your skills, we recommend going
through `Godot's free Getting Started
series <https://docs.godotengine.org/en/stable/getting_started/introduction/index.html>`__
first.

In the series, you will learn how to:

-  Register users and save player session data (authentication).
-  Set up an authoritative server that serves as the central hub for all
   the players.
-  Create a "puppeteer" setup. This is when players send their inputs to
   the server, which takes care of updating the game simulation. This
   setup limits cheating.
-  Use W4's manual matchmaking with lobbies.
-  Use the W4 API to allow multiple players to play together with an
   authoritative server.
-  Build and publish a game using W4 cloud.

The setup you'll learn is suitable for most real-time multiplayer games
that do not rely on sub-second reflexes, such as Overcooked, and
turn-based games like The Witcher's Gwent.

There's a lot to learn when it comes to online multiplayer game
development, and many advanced topics. This Getting Started series does
*not* cover:

-  Predictions and rollback: playing the game locally to simulate lack
   of latency, and correcting the game state when the server sends the
   synchronized state to players.
-  Peer-to-peer or mesh networking: connecting players directly, without
   a server.
-  Deterministic Networking: reproducing a game on all clients through
   inputs alone

W4 Cloud does allow you to support Peer-to-Peer (P2P) networking,
predictions, and rollback. However, these features require
implementations unique to each game, which is beyond the scope of this
Getting Started series.

Our goal here is to familiarize you with the most common pieces of W4's
API.

At the end of this series, you'll find an annex with links to deepen
your networking skills and dive into topics like predictions and
rollback.

In the next part, we'll get started with an overview of W4 Cloud's
authoritative multiplayer.
