.. This document is meant as a TL;DR for developers experienced with 
   web development discovering the W4 services. Feel free to move it.
   It's meant as a page that could be fleshed out as Q&A happens with
   those developers.

Summary of how W4 Cloud works for experienced developers
========================================================

This page summarizes how W4's features work for developers with online
multiplayer experience.

**W4 Cloud offers:**

-  A backend for multiplayer and online games with a no-nonsense Godot
   integration. The workflow in Godot stays mostly the same, but with a
   multiplayer integration that is mostly ready-to-use.
-  A database based on Supabase handled with a Godot Object-Relational
   Mapping (ORM) that maps database rows to custom Godot classes.
-  Authentication through email and password, OAuth, or seamless.

**How the Godot Plugin API works:**

W4 Cloud uses a Godot addon, ``w4gd``, written in GDScript.

GDScript, as of version 4.2, doesn't have promises built-in. So, most
functions in the ``w4gd`` addon return a ``Request`` object. You can
wait for the server response and get a ``Result`` object using ``await``
and by calling the ``async()`` method of the ``Request`` object.

For example:

::

   var result = await W4GD.matchmaker.create_lobby(...).async()

A ``Result`` object can be an error or contain the result data in a
GDScript-compatible format:

-  Call ``Result.is_error()`` to get the error code.
-  Call ``Result.get_data()`` to get data from your query.

**Matchmaking:**

If you are using the automatic matchmaking API, you have to setup a
matchmaking profile on your admin dashboard.

Otherwise, you need to create lobbies. Lobbies are always used, whether
you use lobbies to host games or for automatic matchmaking.

Creating a lobby manually is an API call that returns a GDScript
``Lobby`` object. It contains the Lobby ID, list of players, and
metadata.

Lobbies don't have a name by default: they only have a Universal Unique
Identifier (UUID). It's up to you to assign names to lobbies if you want
to.

To create something like a Lobby name, you can:

-  Create a Database in which you can query the Lobby ID (UUID) and
   retrieve a name from it.
-  Create a ``props`` configuration in the lobby when you are creating
   it, with the name you want. Then, you parse this property on the
   client when showing the lobby.

**Accounts & Profiles:**

A player account is created on signup (when using email) or anonymously 
via ``login_device`` call.

``login_device`` needs a UUID for the user and a "key" for the password.
Both must be created and stored in the client. ``login_device_auto``
does that automatically.

Like lobbies, players don't have the concept of "nickname". You have to
create a new table with User ID <-> User Nickname and set/query for the
username in the client.

**Database & ORM:**

You can create a new table for the database entirely from Godot by using 
the W4RMMapper object.

The W4-addon has an ORM-like API in GDScript. You create new table
classes using GDScript classes and use it as an interface to save or
query data from the database.

To create the table, run the script in the Godot Editor. There is a new
tab "W4" (in the Scene container), you run the script in it and use the
API Key.
